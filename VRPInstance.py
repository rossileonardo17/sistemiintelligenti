import cplex

class VRPInstance:

    def __init__(self, nodeCount, capacity, x_coord, y_coord, demands, veichles):
        """
        Builds an instance of VRPInstance

        Parameters:
        nodeCount (int): The number of nodes of the current instance
        capacity (int): The capacity of the veichles
        x_coord (double[]): An array that contains the x coordinates of the nodes
        y_coord (double[]): An array that contains the y coordinates of the nodes
        demands (double []): An array that contains the demands of each node
        veichles (int): The number of involved veichles
        """
        self.nodeCount = nodeCount
        self.capacity = capacity   
        self.x_coord = x_coord
        self.y_coord = y_coord
        self.demands = demands
        self.veichles = veichles

    def buildModel(self):
        """
        Returns the CPLEX model that represents the VRP problem

        Returns:
        The CPLEX model that represents the VRP problem
        """
        #Instantiate a blank CPLEX model
        model = cplex.Cplex()

        #At this point different types of constraints have to be added:
        #1. Each client must be served by only a veichle
        self.__clientServedByOneVeichle(model)
        #2. The demand of a veichle's served clients doesn not have to exceed the veichle's capacity
        #3. Each route has to pass from the deposit

    def xpos(self, i, j, k):
    #Return

    #MARK: Private functions

    def __clientServedByOneVeichle(self, model):
        """
        Adds to the given model the constraints that allow that only a veichle
        serves a specific client.

        Parameters:
        model (Cplex): The CPLEX model where the constraints have to be added

        Returns:
        The model with the new constraints
        """
        #Populate the array of the right-hand sides and of the senses of the constraints
        rightHandSides = []
        senses = []
        for _ in range(self.nodeCount):
            rightHandSides.append(1.0)
            senses.append("E")

        #Generate the constraints
        constraints = []
        for i in range(self.nodeCount):
            #Define the array that will contain the indexes of the variables envolved by the constraint
            indexes = []
            #Define the array that will contain the coefficients of the variables envolved by the constraint
            values = []
            for k in range(self.veichles):
                for j in range(self.nodeCount):
                    indexes.append(self.xpos(i, j, k))
                    values.append(1.0)
            #Build the constraint
            constraint = [indexes, values]
            #Add the constraint to the list of constraints
            constraints.append(constraint)

        #Add the generated constraints to the model
        model.linear_constraints.add(lin_expr=constraints, senses=senses, rhs=rightHandSides)
