#!/usr/bin/env python
# -*- coding: utf-8 -*- 

from InputParser import InputParser

VERBOSE = 101

parser = InputParser("/Users/leonardorossi/Documents/Università/sistemiintelligenti/data/att48.vrp")
instance = parser.parseDataset()

if (VERBOSE > 100):
    print("### INSTANCE CONTENT ###")
    print("Node count: " + instance.nodeCount)
    print("Veichles capacity: " + instance.capacity)
    print("--- Node coordinates ---")
    for i in range(len(instance.x_coord)):
        print("Node " + str(i+1) + ": (" + str(instance.x_coord[i]) + ", " + str(instance.y_coord[i] ) + ")")
    print("--- Node demands ---")
    for i in range(len(instance.demands)):
        print("Node " + str(i+1) + " " + str(instance.demands[i]))