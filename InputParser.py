from VRPInstance import VRPInstance

class InputParser:

    PARAM_LINE_SEPARATOR = " : "
    DATA_LINE_SEPARATOR = " "
    NODE_COUNT = "DIMENSION"
    CAPACITY = "CAPACITY"
    NODE_COORD_SECTION = "NODE_COORD_SECTION\n"
    DEMAND_SECTION = "DEMAND_SECTION\n"
    DEPOT_SECTION = "DEPOT_SECTION\n"

    def __init__(self, datasetPath):
        """ 
        Builds an instance of an InputParser

        Parameters:
        datasetPath (string): the path of the file where the dataset is stored
        """
        self.datasetPath = datasetPath

    def parseDataset(self):
        """ 
        Parses the given dataset and produces a VRP instance

        Returns:
        A VRP problem instance
        """

        #Open the dataset file
        dataset = open(self.datasetPath, "r")

        #Read each line of the dataset
        nodeCount = 0
        capacity = 0
        isNodeCoordSection = False
        isDemandSection = False
        x_coordinates = []
        y_coordinates = []
        demands = []

        for line in dataset:
            #Check if the current line contains the line separator
            if (self.PARAM_LINE_SEPARATOR in line):
                #Split the line and analyze its parameters
                splitResult = line.split(self.PARAM_LINE_SEPARATOR)
                #Based on the current parameters, set a specific value
                if (splitResult[0] == self.NODE_COUNT):
                    nodeCount = splitResult[1]
                elif (splitResult[0] == self.CAPACITY):
                    capacity = splitResult[1]
            else:
                #If the line does not contain a separator it means
                #that the parameters section is ended and starts the
                #data section of the file
                if (line == self.NODE_COORD_SECTION):
                    isNodeCoordSection = True
                    isDemandSection = False
                    continue
                elif (line == self.DEMAND_SECTION):
                    isDemandSection = True
                    isNodeCoordSection = False
                    continue
                elif (line == self.DEPOT_SECTION):
                    isDemandSection = False
                    isNodeCoordSection = False
            
            #At this point, based on the current secion read the data
            if (isNodeCoordSection):
                splitResult = line.split(self.DATA_LINE_SEPARATOR)
                x_coordinates.append(splitResult[1])
                y_coordinates.append(splitResult[2])
            elif (isDemandSection):
                splitResult = line.split(self.DATA_LINE_SEPARATOR)
                demands.append(splitResult[1])

        #Close the file
        dataset.close()

        #Based on the parsed parameters build an instance of VRP and return it
        return VRPInstance(nodeCount, capacity, x_coordinates, y_coordinates, demands, 3)